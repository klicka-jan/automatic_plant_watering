from rest_framework import serializers
from rest_framework.serializers import Serializer


class WateringRequest:
    ids: list
    duration: int
    delay: int


# class WaterRequestSerializer(Serializer):
#     ids = serializers.ListField(child=serializers.IntegerField())
#     duration = serializers.IntegerField()
#     delay = serializers.IntegerField(required=False)
