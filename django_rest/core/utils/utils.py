import logging
from collections import namedtuple

import simplejson
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework.serializers import ModelSerializer


def auto_str(cls):
    """
    Taken from user @falsetru on https://stackoverflow.com/questions/32910096/is-there-a-way-to-auto-generate-a-str-implementation-in-python
    """

    def __str__(self):
        fields = []
        for v in vars(self).items():
            if v[0] != "_state":
                fields.append(v)
        return '%s{%s}' % (
            type(self).__name__,
            ', '.join('%s:%s' % item for item in fields)
        )

    cls.__str__ = __str__
    return cls


def serialize(class_name, dictionary):
    return namedtuple(class_name, dictionary.keys())(*dictionary.values())


def get_logger() -> logging.Logger:
    return logging.getLogger(__name__)
