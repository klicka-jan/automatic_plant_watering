import time
from datetime import datetime, timedelta, timezone
from typing import Optional

from core.models import WaterPump, WateringRecord, SensorReading
from core.utils.utils import get_logger
from hardware.gpio import Gpio


def water_gpio(pin, duration):
    Gpio.output_pin_blocking(pin, duration, False)


def water(pump: WaterPump, duration=-1) -> Optional[WateringRecord]:
    log = get_logger()
    if duration < 0:
        duration = pump.active_watering_configuration.watering_duration
    if duration < 0:
        log.error('Duration is below 0, not watering!')
        return
    log.info('Watering %s sec with pump %s', duration, pump)
    now = datetime.now()
    water_gpio(pump.gpio_pin_number, duration)
    res = WateringRecord(water_pump=pump, watering_configuration=pump.active_watering_configuration, watered_at=now,
                         watering_duration=duration, estimated_water_ml=0)  # todo estimated water ml
    res.save()
    log.info('Watered.')
    return res


def water_sensor(pump: WaterPump, duration: int) -> Optional[WateringRecord]:
    log = get_logger()
    if duration < 0:
        raise ValueError('Negative duration')

    now = datetime.now(timezone.utc)
    waterings = sorted(pump.watering_records.all(), key=lambda a: a.watered_at, reverse=True)
    last_watering: datetime = waterings[0].watered_at if len(waterings) > 0 else None
    readings = sorted(pump.sensor_readings.all(), key=lambda a: a.read_at, reverse=True)

    last_wet_reading: SensorReading = None
    for reading in readings:
        if reading.is_watered:
            last_wet_reading = reading
            break

    is_watered: bool = make_reading(pump=pump).is_watered

    log.info('Sensor %s is watered %s', pump.sensor_pin, is_watered)

    if last_wet_reading is not None:
        log.info('Last wet reading %s', last_wet_reading)

    if not is_watered and (last_wet_reading is None or now - last_wet_reading.read_at > timedelta(seconds=pump.stay_dry_for_s)):
        log.info('Watering %s sec with pump %s Last watering %s last watering %s', duration, pump, last_watering,
                 last_wet_reading.read_at if last_wet_reading is not None else None)
        water_gpio(pump.gpio_pin_number, duration)
        # SensorReading(water_pump=pump, pin=pump.sensor_pin, raw_result=1, is_watered=True).save() # debug only
        res = WateringRecord(water_pump=pump, watering_configuration=pump.active_watering_configuration, watered_at=now,
                             watering_duration=duration, estimated_water_ml=0)  # todo estimated water ml
        res.save()
        log.info('Watered.')
        return res
    else:
        log.info('Not watering')


def make_reading(pump: WaterPump) -> SensorReading:
    Gpio.output_pin_non_blocking(pump.sensor_power_pin, True)
    time.sleep(0.001)
    sensor_res: int = Gpio.read_pin(pump.sensor_pin)
    Gpio.output_pin_non_blocking(pump.sensor_power_pin, False)

    is_watered: bool = sensor_res == 0
    res = SensorReading(water_pump=pump, pin=pump.sensor_pin, raw_result=sensor_res, is_watered=is_watered)
    get_logger().info('Made sensor reading %s', res)
    res.save()
    return res
