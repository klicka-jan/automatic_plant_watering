from django.contrib.postgres.fields import ArrayField
from django.db import models
from rest_framework.serializers import ModelSerializer

from core.utils.utils import auto_str


@auto_str
class BaseModel(models.Model):
    when_created = models.DateTimeField(auto_now_add=True)
    who_created = models.CharField(max_length=100, default="ichtil")  # todo current user provider
    when_modified = models.DateTimeField(auto_now=True)
    who_modified = models.CharField(max_length=100, default="ichtil")  # todo

    class Meta:
        abstract = True


class WateringConfiguration(BaseModel):
    code = models.CharField(max_length=10, unique=True)
    watering_duration = models.PositiveIntegerField()  # seconds
    watering_times = ArrayField(models.TimeField())


class WateringConfigurationSerializer(ModelSerializer):
    class Meta:
        model = WateringConfiguration
        fields = '__all__'


class Plant(BaseModel):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    type = models.ForeignKey('PlantType', on_delete=models.CASCADE, related_name='plants')
    water_pump = models.ForeignKey('WaterPump', on_delete=models.SET_NULL, null=True, related_name="plants")


class PlantSerializer(ModelSerializer):
    class Meta:
        model = Plant
        fields = '__all__'


class PlantType(BaseModel):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=10, unique=True)
    description = models.CharField(max_length=1000)
    watering_configuration = models.ForeignKey(WateringConfiguration, on_delete=models.SET_NULL, null=True,
                                               related_name="plant_types")


class PlantTypeSerializer(ModelSerializer):
    class Meta:
        model = PlantType
        fields = '__all__'


class WaterPump(BaseModel):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=10, unique=True)
    description = models.CharField(max_length=1000, blank=True)
    url = models.URLField(default="localhost:8000")
    gpio_pin_number = models.PositiveSmallIntegerField()
    active_watering_configuration = models.ForeignKey(WateringConfiguration, on_delete=models.SET_NULL,
                                                      related_name='pumps', null=True)
    sensor_pin = models.PositiveSmallIntegerField(null=True)
    sensor_power_pin = models.PositiveSmallIntegerField(null=True)
    stay_dry_for_s = models.IntegerField(null=False, default=3600*24)


class WaterPumpSerializer(ModelSerializer):
    class Meta:
        model = WaterPump
        fields = '__all__'


class WateringRecord(BaseModel):
    water_pump = models.ForeignKey(WaterPump, on_delete=models.CASCADE, related_name='watering_records')
    watering_configuration = models.ForeignKey(WateringConfiguration, on_delete=models.CASCADE,
                                               related_name='watering_records', null=True)
    watered_at = models.DateTimeField(auto_now=True)
    watering_duration = models.PositiveIntegerField()
    estimated_water_ml = models.PositiveIntegerField()


class SensorReading(BaseModel):
    water_pump = models.ForeignKey(WaterPump, on_delete=models.CASCADE, related_name='sensor_readings', null=True)
    read_at = models.DateTimeField(auto_now=True)
    pin = models.PositiveSmallIntegerField()
    raw_result = models.PositiveSmallIntegerField()
    is_watered = models.BooleanField()
