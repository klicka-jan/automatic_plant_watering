from django.http import HttpResponse
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet, ModelViewSet

from django.apps import apps

from core.utils import watering_utils
from core.models import PlantType, PlantTypeSerializer, WaterPump, WaterPumpSerializer, WateringConfiguration, \
    WateringConfigurationSerializer, PlantSerializer, Plant
from core.utils.utils import serialize, get_logger
from hardware.gpio import Gpio


class PlantTypeController(ModelViewSet):
    queryset = PlantType.objects.all()
    serializer_class = PlantTypeSerializer


class WaterPumpController(ModelViewSet):
    queryset = WaterPump.objects.all()
    serializer_class = WaterPumpSerializer


class WateringConfigurationController(ModelViewSet):
    queryset = WateringConfiguration.objects.all()
    serializer_class = WateringConfigurationSerializer


class PlantController(ModelViewSet):
    queryset = Plant.objects.all()
    serializer_class = PlantSerializer


class WateringController(ViewSet):
    log = get_logger()

    @action(methods=['POST'], detail=False, url_path='now', url_name='now')
    def now(self, request):
        watering_request = serialize("WateringRequest", request.data)
        self.log.info('Watering request: %s', watering_request)
        for water_pump in WaterPump.objects.filter(pk__in=watering_request.ids):
            self.log.info('Watering now - pump %s', water_pump)
            watering_utils.water(water_pump, duration=watering_request.duration)
        return HttpResponse(status=status.HTTP_200_OK)

    @action(methods=['POST'], detail=False, url_path='sensor', url_name='sensor')
    def sensor(self, request):
        watering_request = serialize("WateringRequest", request.data)
        self.log.info('Watering request: %s', watering_request)
        for water_pump in WaterPump.objects.filter(pk__in=watering_request.ids):
            self.log.info('Watering sensor - pump %s', water_pump)
            watering_utils.water_sensor(water_pump, duration=watering_request.duration)
        return HttpResponse(status=status.HTTP_200_OK)

    @action(methods=['POST'], detail=False, url_path='delay', url_name='delay')
    def delay(self, request):
        self.log.info('delay!')
        return Response(WaterPumpSerializer(WaterPump.objects.all()[0]).data)

    @action(methods=['GET'], detail=False, url_path='reschedule', url_name='reschedule')
    def reschedule(self, request):
        apps.get_app_config('core').schedule_waterings()
        return HttpResponse(status=status.HTTP_200_OK)


class SensorController(ViewSet):
    log = get_logger()

    @action(methods=['GET'], detail=False, url_path='water', url_name='water')
    def read_from_water_sensor(self, request):
        Gpio.output_pin_non_blocking(3, True)
        pin_res = Gpio.read_pin(4)
        self.log.info('Reading from water sensor %s', pin_res)
        self.log.info('0==watered')
        Gpio.output_pin_non_blocking(3, False)
        return Response(pin_res)

    # class TestSet(ViewSet):
    #     @action(methods=['POST'], detail=False, url_path='test/(?P<p>[0-9]*)', url_name='test')
    #     def test(self, request, *args, **kwargs):
    #         log.info(kwargs['p'])
    #         return Response(WaterPumpSerializer(WaterPump(code='aaa', name='aaa', gpio_pin_number=123)).data)
    #         p1 = int(kwargs['p1'])
    #         p2 = int(kwargs['p2'])
    #         print(p1, p2)

    # class WaterNowView(APIView):
    #     def get(self, request):
    #         return Response(WaterPumpSerializer(WaterPump(code='aaa', name='aaa', gpio_pin_number=123)).data)
    #
    #     def post(self, request):
    #         print(type(request.data))
    #         serializer = WaterRequestSerializer(data=request.data)
    #         serializer.is_valid(raise_exception=True)
    #         print(serializer.validated_data)
