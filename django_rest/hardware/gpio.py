import logging
import os

if os.name == 'nt':
    import hardware.windows_gpio as GPIO
else:
    import RPi.GPIO as GPIO

from time import sleep

log = logging.getLogger(__name__)


class Gpio:
    gpio_ready = False

    @classmethod
    def init(cls, force=False):
        """
        Init doesn't have to be called, it will be called automatically (if needed) when preparing pin
        """
        if Gpio.gpio_ready and not force:
            return

        GPIO.setmode(GPIO.BCM)
        Gpio.gpio_ready = True

    @classmethod
    def cleanup(cls):
        """
            Cleanup must be called manually after all io operations are finished.
        """
        GPIO.cleanup()
        Gpio.gpio_ready = False

    @classmethod
    def prepare_pin(cls, pin, pin_mode_out):
        """
        pin_mode_out - true for out
        There shouldn't be a need to call this function outside of this class.
        """
        if not Gpio.gpio_ready:
            Gpio.init()

        pin_mode = Gpio.resolve_pin_mode(pin_mode_out)
        GPIO.setup(pin, pin_mode)

    @classmethod
    def output_pin_blocking(cls, pin, duration, pin_mode_high):
        """
        duration - seconds
        pin_mode_high - true for GPIO.HIGH
        """
        Gpio.prepare_pin(pin, True)
        GPIO.output(pin, Gpio.resolve_value(pin_mode_high))
        sleep(duration)
        GPIO.output(pin, Gpio.resolve_opposite_value(pin_mode_high))

    @classmethod
    def output_pin_non_blocking(cls, pin, pin_mode_high):
        Gpio.prepare_pin(pin, True)
        GPIO.output(pin, Gpio.resolve_value(pin_mode_high))

    @classmethod
    def read_pin(cls, pin):
        Gpio.prepare_pin(pin, False)
        return GPIO.input(pin)

    @classmethod
    def resolve_value(cls, high):
        return GPIO.HIGH if high else GPIO.LOW

    @classmethod
    def resolve_opposite_value(cls, high):
        return GPIO.LOW if high else GPIO.HIGH

    @classmethod
    def resolve_pin_mode(cls, out):
        return GPIO.OUT if out else GPIO.IN

    @classmethod
    def resolve_opposite_pin_mode(cls, out):
        return GPIO.IN if out else GPIO.OUT
