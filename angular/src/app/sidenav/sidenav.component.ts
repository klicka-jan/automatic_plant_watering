import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {controlPanel, dashboard, dmRoutes, visibleRoutes} from '../routes/routes'
import {SpinnerService} from "../services/spinner.service";

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  dashboardRoute = dashboard;
  controlPanelRoute = controlPanel;
  dmRoutes = dmRoutes;

  openSidenav: boolean = false;

  checkOpenSidenav(width) {
    this.openSidenav = width >= 600;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.checkOpenSidenav(window.innerWidth);
  }

  constructor() {
  }

  ngOnInit() {
    this.onResize(null);
    console.log(visibleRoutes) // todo : ?
  }
}
