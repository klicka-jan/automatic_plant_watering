import {DashboardComponent} from "../dashboard/dashboard.component";
import {ControlPanelComponent} from "../control-panel/control-panel.component";
import {PlantTypeComponent} from "../dm/plant-type/plant-type.component";
import {WaterPumpComponent} from "../dm/water-pump/water-pump.component";
import {PlantTypeListComponent} from "../dm/plant-type-list/plant-type-list.component";
import {WaterPumpListComponent} from "../dm/water-pump-list/water-pump-list.component";
import {WateringConfigurationComponent} from "../dm/watering-configuration/watering-configuration.component";
import {WateringConfigurationListComponent} from "../dm/watering-configuration-list/watering-configuration-list.component";

export class Route {
  path: string;
  name: string;
  component: any;
}

export const dashboard: Route = {path: '', name: 'Dashboard', component: DashboardComponent};
export const controlPanel: Route = {path: 'control-panel', name: 'Control panel', component: ControlPanelComponent};

export const singularRoutes: Route[] = [dashboard, controlPanel];
export const dmRoutes: Route[] = [
  {path: 'plant-types', name: 'Plant types', component: PlantTypeListComponent},
  {path: 'water-pumps', name: 'Water pumps', component: WaterPumpListComponent},
  {path: 'watering-configurations', name: 'Watering configurations', component: WateringConfigurationListComponent},
];
export const nonVisibleRoutes: Route[] = [
  {path: 'plant-type', name: 'Plant type', component: PlantTypeComponent},
  {path: 'plant-type/:id', name: 'Plant type', component: PlantTypeComponent},
  {path: 'water-pump', name: 'Water pump', component: WaterPumpComponent},
  {path: 'water-pump/:id', name: 'Water pump', component: WaterPumpComponent},
  {path: 'watering-configuration', name: 'Watering configuration', component: WateringConfigurationComponent},
  {path: 'watering-configuration/:id', name: 'Watering configuration', component: WateringConfigurationComponent},
];

export const visibleRoutes: Route[] = [...dmRoutes, ...singularRoutes];
export const allRoutes: Route[] = [...visibleRoutes, ...nonVisibleRoutes];
