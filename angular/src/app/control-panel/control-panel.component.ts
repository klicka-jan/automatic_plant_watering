import {Component, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {SpinnerService} from "../services/spinner.service";
import {WaterPump} from "../dm/models/water-pump";

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.css']
})
export class ControlPanelComponent implements OnInit {
  pendingRequest: boolean = false;

  waterOneTimeFormGroup = this.formBuilder.group({
    'wateringDurationInput': [null, [
      Validators.required,
      Validators.min(1),
      Validators.max(60),
    ]],
  });

  waterWithDelayFormGroup = this.formBuilder.group({
    'wateringDurationInput': [null, [
      Validators.required,
      Validators.min(1),
      Validators.max(60)
    ]],
    'wateringDelayInput': [null, [
      Validators.min(1),
    ]],
  });

  pumps: WaterPump[] = [];
  selectedPumps: WaterPump[] = [];
  useSensor: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private http: HttpClient) {
  }

  ngOnInit() {
    this.pendingRequest = true;
    this.http.get<WaterPump[]>('api/water-pump')
      .subscribe(next => {
        console.log(next);
        this.pumps = next;
        this.pendingRequest = false;
      }, error => {
        console.error('error', error);
        this.pendingRequest = false;
      })
  }

  onWaterNow() {
    this.pendingRequest = true;
    const url = this.useSensor ? 'api/watering/sensor' : 'api/watering/now';
    this.http.post<any>(url, {
      ids: this.selectedPumps,
      duration: this.waterOneTimeFormGroup.value['wateringDurationInput']
    }).subscribe(next => {
      console.log(next);
      this.pendingRequest = false;
    }, error => {
      console.error(error);
      this.pendingRequest = false;
    });
  }

  selectionChange($event) {
    // for future combos
  }

  onWaterWithDelay() {
  }

  getWaterNowErrorMessage(formGroup, formInput) {
    if (formGroup.get(formInput).valid) {
      return "No error";
    } else if (formGroup.get(formInput).hasError('required')) {
      return "This is a required value";
    } else if (formGroup.get(formInput).hasError('min')) {
      return "This is value is too low";
    } else if (formGroup.get(formInput).hasError('max')) {
      return "This is value is too big";
    } else {
      return "";
    }
  }

  reschedule() {
    this.http.get('api/watering/reschedule')
      .subscribe(data => {
        console.log(data)
      })
  }
}
