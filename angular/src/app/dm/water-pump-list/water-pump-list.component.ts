import {Component, OnInit, ViewChild} from '@angular/core';
import {WaterPump} from '../models/water-pump';
import {MatPaginator} from "@angular/material/paginator";
import {HttpClient} from "@angular/common/http";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-water-pump-list',
  templateUrl: './water-pump-list.component.html',
  styleUrls: ['./water-pump-list.component.css']
})
export class WaterPumpListComponent implements OnInit {
  displayedColumns: string[] = ['id', 'code', 'name', 'stay_dry_for_s', 'gpio_pin_number', 'sensor_pin', 'sensor_power_pin', 'actions'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  dataSource = new MatTableDataSource<WaterPump>();

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.http.get<WaterPump[]>('api/water-pump').subscribe(
      next => {
        console.log(next);
        this.dataSource.data = next;
        this.dataSource.paginator = this.paginator;
      }, error => {
        console.log('error');
      }
    );
  }

  delete(id) {
    this.http.delete('api/water-pump/' + id)
      .subscribe(next => {
        console.log(next);
        this.fetchData();
      }, error => console.error(error));
  }

}
