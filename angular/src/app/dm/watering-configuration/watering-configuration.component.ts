import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {WateringConfiguration, WaterPump} from "../models/water-pump";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-watering-configuration',
  templateUrl: './watering-configuration.component.html',
  styleUrls: ['./watering-configuration.component.css']
})
export class WateringConfigurationComponent implements OnInit {
  pendingRequest: boolean = false;

  form = this.formBuilder.group({
    watering_duration: [null, [
      Validators.required,
      Validators.min(1),
      Validators.max(60),
    ]],
    code: [null, [Validators.required, Validators.maxLength(10), Validators.minLength(3)]],
    // watering_time: [null, [Validators.pattern()]]
    // todo optional: types
  });
  instanceId: number;
  watering_times: string[] = [];

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private http: HttpClient) {
  }

  ngOnInit() {
    if (this.route.routeConfig.component.name == this.constructor.name) {
      this.route.params.subscribe((params) => this.instanceId = params['id']);
    }
    if (this.instanceId) {
      this.http.get<WateringConfiguration>('api/watering-configuration/' + this.instanceId)
        .subscribe(data => {
          console.log(data);
          this.form.patchValue({
            watering_duration: data.watering_duration,
            code: data.code,
          });
          this.watering_times = data.watering_times
        }, error => console.error(error))
    }
  }

  onSubmit() {
    let body = this.form.getRawValue();
    body['watering_times'] = this.watering_times
    const req_body = JSON.stringify(body);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    console.log('posting ', req_body);
    if (this.instanceId) {
      this.http.put("api/watering-configuration/" + this.instanceId, req_body, httpOptions)
        .subscribe(next => console.log(next), error => console.error(error));
    } else {
      this.http.post("api/watering-configuration", req_body, httpOptions)
        .subscribe(next => console.log(next), error => console.error(error));
    }
  }

  addWateringTime(time: string) {
    this.watering_times.push(time)
  }
}
