import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, ValidatorFn, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";
import {WateringConfiguration, WaterPump} from "../models/water-pump";

@Component({
  selector: 'app-water-pump',
  templateUrl: './water-pump.component.html',
  styleUrls: ['./water-pump.component.css']
})
export class WaterPumpComponent implements OnInit {
  pinValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      return [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27].includes(control.value)
        ? null
        : {'pin': 'invalid'};
    };
  }

  // todo reload on create
  form = this.fb.group({
    name: [null, [Validators.required, Validators.maxLength(100)]],
    code: [null, [Validators.required, Validators.maxLength(10), Validators.minLength(3)]],
    gpio_pin_number: [null, [Validators.required, this.pinValidator()]],
    sensor_pin: [null, [this.pinValidator()]],
    sensor_power_pin: [null, [this.pinValidator()]],
    stay_dry_for_s: [null, [Validators.min(0)]],
    active_watering_configuration: [null],
    description: [null, Validators.maxLength(1000)],
  });
  pendingRequest: boolean = false;
  instanceId = undefined;
  watering_configurations: WateringConfiguration[];

  constructor(private fb: FormBuilder,
              private http: HttpClient,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    if (this.route.routeConfig.component.name == this.constructor.name) {
      this.route.params.subscribe((params) => this.instanceId = params['id']);
    }
    if (this.instanceId) {
      this.http.get<WaterPump>('api/water-pump/' + this.instanceId)
        .subscribe(data => {
          console.log(data);
          this.form.patchValue({
            name: data.name,
            code: data.code,
            description: data.description,
            gpio_pin_number: data.gpio_pin_number,
            sensor_pin: data.sensor_pin,
            sensor_power_pin: data.sensor_power_pin,
            stay_dry_for_s: data.stay_dry_for_s,
            active_watering_configuration: data.active_watering_configuration
          });
        }, error => console.error(error))
    }
    this.http.get<WateringConfiguration[]>('api/watering-configuration')
      .subscribe(data2 => {
        console.log(data2);
        this.watering_configurations = data2;
      });
  }

  onSubmit() {
    // todo snackbar
    if (this.form.value['description'] === null) {
      this.form.value['description'] = "";
    }
    if (this.instanceId) {
      this.http.put("api/water-pump/" + this.instanceId, this.form.value)
        .subscribe(next => console.log(next), error => console.error(error));
    } else {
      this.http.post("api/water-pump", this.form.value)
        .subscribe(next => console.log(next), error => console.error(error));
    }
  }
}
