import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {FormBuilder, Validators} from "@angular/forms";
import {PlantType, WateringConfiguration, WaterPump} from "../models/water-pump";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-plant-type',
  templateUrl: './plant-type.component.html',
  styleUrls: ['./plant-type.component.css']
})
export class PlantTypeComponent implements OnInit {
  // todo reload on create
  form = this.fb.group({
    name: [null, [Validators.required, Validators.maxLength(100)]],
    code: [null, [Validators.required, Validators.maxLength(10), Validators.minLength(3)]],
    description: [null, Validators.maxLength(1000)],
    watering_configuration: [null],
  });
  pendingRequest: boolean = false;
  instanceId = undefined;
  watering_configurations: WateringConfiguration[];

  constructor(private fb: FormBuilder,
              private http: HttpClient,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    if (this.route.routeConfig.component.name == this.constructor.name) {
      this.route.params.subscribe((params) => this.instanceId = params['id']);
    }
    if (this.instanceId) {
      this.http.get<PlantType>('api/plant-type/' + this.instanceId)
        .subscribe(data => {
          console.log(data);
          this.form.patchValue({
            name: data.name,
            code: data.code,
            description: data.description,
            watering_configuration: data.watering_configuration.id,
          });
        }, error => console.error(error))
    }
    this.http.get<WateringConfiguration[]>('api/watering-configuration')
      .subscribe(data2 => {
        console.log(data2);
        this.watering_configurations = data2;
      });
  }

  onSubmit() {
    if (this.form.value['description'] === null) {
      this.form.value['description'] = "";
    }
    if (this.instanceId) {
      this.http.put("api/plant-type/" + this.instanceId, this.form.value)
        .subscribe(next => console.log(next), error => console.error(error));
    } else {
      this.http.post("api/plant-type", this.form.value)
        .subscribe(next => console.log(next), error => console.error(error));
    }
  }
}
